<?php 
	
	require_once "clases/Conexion.php";
	$obj= new conectar();
	$conexion=$obj->conexion();

	$sql="SELECT * from Usuariosbd where email='admin'";
	$result=mysqli_query($conexion,$sql);
	$validar=0;
	
	
 ?>


<!DOCTYPE html>
<html>
<head>
	<title>!TeSirveTeLoRegalo!</title>
	<link rel="stylesheet" type="text/css" href="librerias/bootstrap/css/bootstrap.css">
	<script src="librerias/jquery-3.2.1.min.js"></script>
	<script src="js/funciones.js"></script>
</head>
<body style="background-color: lightgray">
	<br><br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-3" class="responsive">
				<div class="panel panel-primary">
					
					<div class="panel panel-body">
						<p>
							<img src="img/img.png" class="responsive" height="230" >
						</p>
						<form id="frmLogin">
							<label style="margin-left:60px">Ingrese Rut</label>
							<input type="text" class="form-control input-sm" name="usuario" id="usuario">
							<label style="margin-left:60px" >Ingrese Contraseña</label>
							<input type="password" name="password" id="password" class="form-control input-sm">
							<p></p>
							<span class="btn btn-info btn-sm btn-block" id="entrarSistema">Iniciar Sesion</span>
							<?php  if(!$validar): ?>
							<a href="registro.php" class="btn btn-warning btn-sm btn-block">Crear Cuentar</a>
							<?php endif; ?>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('#entrarSistema').click(function(){

		vacios=validarFormVacio('frmLogin');

			if(vacios > 0){
				alert("Complete todos los campos");
				return false;
			}

		datos=$('#frmLogin').serialize();
		$.ajax({
			type:"POST",
			data:datos,
			url:"procesos/regLogin/login.php",
			success:function(r){

				if(r==1){
					window.location="vistas/inicio.php";
				}else{
					alert("Error");
				}
			}
		});
	});
	});
</script>